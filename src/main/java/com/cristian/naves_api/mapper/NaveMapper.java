package com.cristian.naves_api.mapper;

import com.cristian.naves_api.dto.NaveDTO;
import com.cristian.naves_api.model.Nave;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface NaveMapper {
    NaveMapper INSTANCE = Mappers.getMapper(NaveMapper.class);

    NaveDTO toNaveDTO(Nave nave);

    Nave toNave(NaveDTO naveDTO);
}