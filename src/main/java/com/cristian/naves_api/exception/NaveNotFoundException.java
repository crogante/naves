package com.cristian.naves_api.exception;

public class NaveNotFoundException extends RuntimeException {
    public NaveNotFoundException(Long id) {
        super("Nave no encontrada para ID: " + id);
    }
}

