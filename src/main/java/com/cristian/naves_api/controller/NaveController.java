package com.cristian.naves_api.controller;

import com.cristian.naves_api.dto.NaveDTO;
import com.cristian.naves_api.service.NaveService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/naves")
public class NaveController {

    private final NaveService naveService;

    @Autowired
    public NaveController(NaveService naveService) {
        this.naveService = naveService;
    }

    @Operation(summary = "Obtener todas las naves")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the naves",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = NaveDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Naves not found",
                    content = @Content) })
    @GetMapping
    public ResponseEntity<List<NaveDTO>> getAllNaves() {
        return ResponseEntity.ok(naveService.getAllNaves());
    }

    @Operation(summary = "Obtener todas las naves paginadas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the naves",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = NaveDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Naves not found",
                    content = @Content) })
    @GetMapping("/listar")
    public ResponseEntity<Page<NaveDTO>> getAllNavesPaginado(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(naveService.getAllNavesPaginado(pageable));
    }

    @Operation(summary = "Buscar naves por nombre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the naves",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = NaveDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Naves not found",
                    content = @Content) })
    @GetMapping("/buscar")
    public ResponseEntity<Page<NaveDTO>> buscarPorNombre(@RequestParam String nombre, Pageable pageable) {
        return ResponseEntity.ok(naveService.getAllNavesPorNombre(nombre, pageable));
    }

    @Operation(summary = "Crear una nueva nave")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the nave",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = NaveDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input",
                    content = @Content) })
    @PostMapping
    public ResponseEntity<NaveDTO> createNave(@RequestBody NaveDTO naveDTO) {
        return ResponseEntity.ok(naveService.createNave(naveDTO));
    }

    @Operation(summary = "Actualizar una nave existente")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the nave",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = NaveDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid input",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Nave not found",
                    content = @Content) })
    @PutMapping("/{id}")
    public ResponseEntity<NaveDTO> updateNave(@PathVariable Long id, @RequestBody NaveDTO naveDTO) {
        return ResponseEntity.ok(naveService.updateNave(id, naveDTO));
    }

    @Operation(summary = "Obtener una nave por ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the nave",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = NaveDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid ID",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Nave not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<NaveDTO> getNaveById(@PathVariable Long id) {
        return ResponseEntity.ok(naveService.getNaveById(id));
    }

    @Operation(summary = "Eliminar una nave por ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the nave",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid ID",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Nave not found",
                    content = @Content) })
    @DeleteMapping("/{id}")
    public void deleteNave(@PathVariable Long id) {
        naveService.deleteNave(id);
    }
}
