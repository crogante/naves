package com.cristian.naves_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NavesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(NavesApiApplication.class, args);
	}

}
