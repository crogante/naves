package com.cristian.naves_api.service;

import com.cristian.naves_api.dto.NaveDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface NaveService {
    List<NaveDTO> getAllNaves();
    NaveDTO getNaveById(Long id);
    NaveDTO createNave(NaveDTO naveDTO);
    NaveDTO updateNave(Long id, NaveDTO naveDTO);
    void deleteNave(Long id);
    Page<NaveDTO> getAllNavesPaginado(Pageable pageable);
    Page<NaveDTO> getAllNavesPorNombre(String nombre, Pageable pageable);
}
