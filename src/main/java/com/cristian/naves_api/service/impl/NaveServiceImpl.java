package com.cristian.naves_api.service.impl;

import com.cristian.naves_api.dto.NaveDTO;
import com.cristian.naves_api.exception.NaveNotFoundException;
import com.cristian.naves_api.mapper.NaveMapper;
import com.cristian.naves_api.model.Nave;
import com.cristian.naves_api.repository.NaveRepository;
import com.cristian.naves_api.service.NaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Service
public class NaveServiceImpl implements NaveService {

    private final NaveRepository naveRepository;
    private final NaveMapper naveMapper = NaveMapper.INSTANCE;
    private KafkaTemplate<String, String> kafkaTemplate;
    @Value("${spring.kafka.topic-name}")
    private String topicName;

    public NaveServiceImpl(NaveRepository naveRepository, KafkaTemplate<String, String> kafkaTemplate) {
        this.naveRepository = naveRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public List<NaveDTO> getAllNaves() {
        Iterable<Nave> naves = naveRepository.findAll();
        return StreamSupport.stream(naves.spliterator(), false)
                .map(naveMapper::toNaveDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable(value = "miCache", key = "#id")
    public NaveDTO getNaveById(Long id) {
        Optional<Nave> optionalNave = naveRepository.findById(id);
        if (optionalNave.isPresent()) {
            return naveMapper.toNaveDTO(optionalNave.get());
        } else {
            throw new NaveNotFoundException(id);
        }
    }

    @Override
    public NaveDTO createNave(NaveDTO naveDTO) {
        Nave nave = new Nave(naveDTO.getName(), naveDTO.getSeries(), naveDTO.getDescription());
        Nave savedNave = naveRepository.save(nave);

        kafkaTemplate.send(topicName, "Nueva nave creada: " + savedNave.getName());

        return naveMapper.toNaveDTO(savedNave);
    }

    @Override
    public NaveDTO updateNave(Long id, NaveDTO naveDTO) {
        Optional<Nave> optionalNave = naveRepository.findById(id);
        if (optionalNave.isPresent()) {
            Nave naveToUpdate = optionalNave.get();
            if (null != naveDTO.getName()) naveToUpdate.setName(naveDTO.getName());
            if (null != naveDTO.getSeries()) naveToUpdate.setSeries(naveDTO.getSeries());
            if (null != naveDTO.getDescription()) naveToUpdate.setDescription(naveDTO.getDescription());
            Nave savedNave = naveRepository.save(naveToUpdate);
            return naveMapper.toNaveDTO(savedNave);
        } else {
            throw new NaveNotFoundException(id);
        }
    }

    @Override
    public void deleteNave(Long id) {
        naveRepository.deleteById(id);
    }

    @Override
    public Page<NaveDTO> getAllNavesPaginado(Pageable pageable) {
        Page<Nave> naves = naveRepository.findAll(pageable);
        return naves.map(this::convertToDTO);
    }

    private NaveDTO convertToDTO(Nave nave) {
        return new NaveDTO(nave.getId(), nave.getName(), nave.getSeries(), nave.getDescription());
    }

    @Override
    public Page<NaveDTO> getAllNavesPorNombre(String nombre, Pageable pageable) {
        Page<Nave> naves = naveRepository.findByNameContaining(nombre, pageable);
        return naves.map(this::convertToDTO);
    }
}
