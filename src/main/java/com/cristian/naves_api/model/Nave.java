package com.cristian.naves_api.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "naves")
public class Nave {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;
        @Column(name = "nombre", nullable = false, length = 100)
        private String name;
        @Column(name = "serie", nullable = false)
        private String series;
        @Column(name = "descripcion", nullable = false)
        private String description;

        public Nave(String name, String series, String description) {
                this.name = name;
                this.series = series;
                this.description = description;
        }
}
