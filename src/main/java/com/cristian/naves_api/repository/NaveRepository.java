package com.cristian.naves_api.repository;

import com.cristian.naves_api.dto.NaveDTO;
import com.cristian.naves_api.model.Nave;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface NaveRepository extends CrudRepository<Nave, Long> {
    Page<Nave> findAll(Pageable pageable);
    Page<Nave> findByNameContaining(String name, Pageable pageable);
}
