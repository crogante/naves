package com.cristian.naves_api.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Before("execution(* com.cristian.naves_api.controller.NaveController.getNaveById(..)) && args(id)")
    public void logBefore(JoinPoint joinPoint, Long id) {
        if (id < 0) {
            logger.warn("Se está intentando buscar una nave con ID negativo: " + id);
        }
    }
}