CREATE TABLE naves (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    serie VARCHAR(255) NOT NULL,
    descripcion VARCHAR(255) NOT NULL
);

INSERT INTO naves (nombre, serie, descripcion) VALUES ('X-Wing', 'Star Wars', 'Nave 1');
INSERT INTO naves (nombre, serie, descripcion) VALUES ('Enterprise', 'Star Trek', 'Nave 2');
