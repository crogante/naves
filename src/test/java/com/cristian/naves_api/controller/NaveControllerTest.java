package com.cristian.naves_api.controller;

import com.cristian.naves_api.dto.NaveDTO;
import com.cristian.naves_api.exception.GlobalExceptionHandler;
import com.cristian.naves_api.exception.NaveNotFoundException;
import com.cristian.naves_api.service.NaveService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class NaveControllerTest {

    @Mock
    private NaveService naveService;

    @InjectMocks
    private NaveController naveController;

    private MockMvc mockMvc;

    private NaveDTO naveDTO;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(naveController)
                .setControllerAdvice(new GlobalExceptionHandler())
                .build();

        naveDTO = new NaveDTO(1L, "Nave1", "Series1", "Description1");
    }

    @Test
    void getAllNaves() throws Exception {
        when(naveService.getAllNaves()).thenReturn(List.of(naveDTO));

        mockMvc.perform(get("/naves"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value(naveDTO.getName()))
                .andDo(print());

        verify(naveService, times(1)).getAllNaves();
    }

    @Test
    void createNave() throws Exception {
        when(naveService.createNave(any(NaveDTO.class))).thenReturn(naveDTO);

        mockMvc.perform(post("/naves")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Nave1\", \"series\": \"Series1\", \"description\": \"Description1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(naveDTO.getName()))
                .andDo(print());

        verify(naveService, times(1)).createNave(any(NaveDTO.class));
    }


    @Test
    void updateNave() throws Exception {
        when(naveService.updateNave(eq(naveDTO.getId()), any(NaveDTO.class))).thenReturn(naveDTO);

        mockMvc.perform(put("/naves/{id}", naveDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Nave1\", \"series\": \"Series1\", \"description\": \"Description1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(naveDTO.getName()))
                .andDo(print());

        verify(naveService, times(1)).updateNave(eq(naveDTO.getId()), any(NaveDTO.class));
    }

    @Test
    void getNaveById() throws Exception {
        when(naveService.getNaveById(naveDTO.getId())).thenReturn(naveDTO);

        mockMvc.perform(get("/naves/{id}", naveDTO.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(naveDTO.getName()))
                .andDo(print());

        verify(naveService, times(1)).getNaveById(naveDTO.getId());
    }

    @Test
    void deleteNave() throws Exception {
        doNothing().when(naveService).deleteNave(naveDTO.getId());

        mockMvc.perform(delete("/naves/{id}", naveDTO.getId()))
                .andExpect(status().isOk())
                .andDo(print());

        verify(naveService, times(1)).deleteNave(naveDTO.getId());
    }

    @Test
    void getNaveById_NaveNotFoundException() throws Exception {
        when(naveService.getNaveById(naveDTO.getId())).thenThrow(new NaveNotFoundException(naveDTO.getId()));

        mockMvc.perform(get("/naves/{id}", naveDTO.getId()))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Nave no encontrada para ID: " + naveDTO.getId()))
                .andDo(print())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof NaveNotFoundException));
    }


    @Test
    void updateNave_NaveNotFoundException() throws Exception {
        Long naveId = 1L;
        NaveDTO naveDTO = new NaveDTO();
        naveDTO.setName("UpdatedName");
        naveDTO.setSeries("UpdatedSeries");
        naveDTO.setDescription("UpdatedDescription");

        when(naveService.updateNave(eq(naveId), any(NaveDTO.class))).thenThrow(new NaveNotFoundException(naveId));

        mockMvc.perform(put("/naves/{id}", naveId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"UpdatedName\", \"series\": \"UpdatedSeries\", \"description\": \"UpdatedDescription\"}"))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof NaveNotFoundException))
                .andExpect(result -> Assertions.assertEquals("Nave no encontrada para ID: " + naveId, result.getResolvedException().getMessage()))
                .andDo(print());

        verify(naveService, times(1)).updateNave(eq(naveId), any(NaveDTO.class));
    }

}