package com.cristian.naves_api.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.cristian.naves_api.dto.NaveDTO;
import com.cristian.naves_api.model.Nave;
import com.cristian.naves_api.repository.NaveRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;

import java.util.Optional;

@SpringBootTest
@EnableCaching
public class CacheIntegrationTest {

    @Autowired
    private NaveService naveService;

    @MockBean
    private NaveRepository naveRepository;

    @Autowired
    private CacheManager cacheManager;

    @Test
    public void testCacheable() {
        Long id = 1L;
        Nave nave = new Nave();
        Mockito.when(naveRepository.findById(id)).thenReturn(Optional.of(nave));

        NaveDTO nave1 = naveService.getNaveById(id);
        assertNotNull(nave1);

        // Verificar que se llamó al repositorio una vez
        verify(naveRepository, times(1)).findById(id);

        NaveDTO nave2 = naveService.getNaveById(id);
        assertNotNull(nave2);

        // Verificar que no se vuelve a llamar al repositorio (devuelve 1 llamada nuevamente)
        verify(naveRepository, times(1)).findById(id);

        assertTrue(nave1 == nave2); // Verificamos que son la misma instancia
    }
}
