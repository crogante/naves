package com.cristian.naves_api.service;

import com.cristian.naves_api.dto.NaveDTO;
import com.cristian.naves_api.exception.NaveNotFoundException;
import com.cristian.naves_api.mapper.NaveMapper;
import com.cristian.naves_api.model.Nave;
import com.cristian.naves_api.repository.NaveRepository;
import com.cristian.naves_api.service.impl.NaveServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class NaveServiceImplTest {

    @Mock
    private NaveRepository naveRepository;

    @InjectMocks
    private NaveServiceImpl naveService;

    @Mock
    private KafkaTemplate<String, String> kafkaTemplate;

    private Nave nave;
    private NaveDTO naveDTO;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        naveService = new NaveServiceImpl(naveRepository, kafkaTemplate);

        nave = new Nave(1L, "Nave1", "Series1", "Description1");
        naveDTO = NaveMapper.INSTANCE.toNaveDTO(nave);
    }

    @Test
    void getAllNaves() {
        when(naveRepository.findAll()).thenReturn(Arrays.asList(nave));

        List<NaveDTO> result = naveService.getAllNaves();

        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertEquals(naveDTO.getName(), result.get(0).getName());
        verify(naveRepository, times(1)).findAll();
    }

    @Test
    void getNaveById() {
        when(naveRepository.findById(nave.getId())).thenReturn(Optional.of(nave));

        NaveDTO result = naveService.getNaveById(nave.getId());

        assertNotNull(result);
        assertEquals(naveDTO.getName(), result.getName());
        verify(naveRepository, times(1)).findById(nave.getId());
    }

    @Test
    void createNave() {
        when(naveRepository.save(any(Nave.class))).thenReturn(nave);

        NaveDTO result = naveService.createNave(naveDTO);

        assertNotNull(result);
        assertEquals(naveDTO.getName(), result.getName());
        verify(naveRepository, times(1)).save(any(Nave.class));
        verify(kafkaTemplate, times(1)).send(any(), any());
    }

    @Test
    void updateNave() {
        when(naveRepository.findById(nave.getId())).thenReturn(Optional.of(nave));
        when(naveRepository.save(any(Nave.class))).thenReturn(nave);

        NaveDTO result = naveService.updateNave(nave.getId(), naveDTO);

        assertNotNull(result);
        assertEquals(naveDTO.getName(), result.getName());
        verify(naveRepository, times(1)).findById(nave.getId());
        verify(naveRepository, times(1)).save(any(Nave.class));
    }

    @Test
    void deleteNave() {
        doNothing().when(naveRepository).deleteById(nave.getId());

        naveService.deleteNave(nave.getId());

        verify(naveRepository, times(1)).deleteById(nave.getId());
    }

    @Test
    void getNaveById_NotFound() {
        when(naveRepository.findById(nave.getId())).thenReturn(Optional.empty());

        assertThrows(NaveNotFoundException.class, () -> {
            naveService.getNaveById(nave.getId());
        });

        verify(naveRepository, times(1)).findById(nave.getId());
    }

    @Test
    void updateNave_NotFound() {
        when(naveRepository.findById(nave.getId())).thenReturn(Optional.empty());

        assertThrows(NaveNotFoundException.class, () -> {
            naveService.updateNave(nave.getId(), naveDTO);
        });

        verify(naveRepository, times(1)).findById(nave.getId());
        verify(naveRepository, times(0)).save(any(Nave.class));
    }

}

