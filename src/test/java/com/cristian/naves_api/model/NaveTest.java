package com.cristian.naves_api.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class NaveTest {

    @Test
    void testGettersAndSetters() {
        Nave nave = new Nave();
        nave.setId(1L);
        nave.setName("Nave1");

        assertEquals(1L, nave.getId());
        assertEquals("Nave1", nave.getName());
    }

    @Test
    void testEqualsAndHashCode() {
        Nave nave1 = new Nave();
        nave1.setId(1L);
        Nave nave2 = new Nave();
        nave2.setId(1L);

        assertEquals(nave1, nave2);
        assertEquals(nave1.hashCode(), nave2.hashCode());
    }

    @Test
    void testToString() {
        Nave nave = new Nave();
        nave.setId(1L);
        nave.setName("Nave1");

        assertTrue(nave.toString().contains("Nave1"));
    }
}
