package com.cristian.naves_api.dto;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class NaveDTOTest {

    @Test
    void testGettersAndSetters() {
        NaveDTO naveDTO = new NaveDTO();
        naveDTO.setId(1L);
        naveDTO.setName("NaveDTO1");

        assertEquals(1L, naveDTO.getId());
        assertEquals("NaveDTO1", naveDTO.getName());
    }

    @Test
    void testEqualsAndHashCode() {
        NaveDTO naveDTO1 = new NaveDTO();
        naveDTO1.setId(1L);
        NaveDTO naveDTO2 = new NaveDTO();
        naveDTO2.setId(1L);

        assertEquals(naveDTO1, naveDTO2);
        assertEquals(naveDTO1.hashCode(), naveDTO2.hashCode());
    }

    @Test
    void testToString() {
        NaveDTO naveDTO = new NaveDTO();
        naveDTO.setId(1L);
        naveDTO.setName("NaveDTO1");

        assertTrue(naveDTO.toString().contains("NaveDTO1"));
    }
}
