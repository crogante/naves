package com.cristian.naves_api.mapper;

import com.cristian.naves_api.dto.NaveDTO;
import com.cristian.naves_api.model.Nave;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NaveMapperTest {

    private NaveMapper naveMapper;

    @BeforeEach
    void setUp() {
        naveMapper = NaveMapper.INSTANCE;
    }

    @Test
    void testToNaveDTO() {
        Nave nave = new Nave(1L, "Nave 1", "Series 1", "Descripción 1");

        NaveDTO expectedDTO = new NaveDTO();
        expectedDTO.setId(1L);
        expectedDTO.setName("Nave 1");
        expectedDTO.setSeries("Series 1");
        expectedDTO.setDescription("Descripción 1");

        NaveDTO resultDTO = naveMapper.toNaveDTO(nave);

        assertEquals(expectedDTO.getId(), resultDTO.getId());
        assertEquals(expectedDTO.getName(), resultDTO.getName());
        assertEquals(expectedDTO.getSeries(), resultDTO.getSeries());
        assertEquals(expectedDTO.getDescription(), resultDTO.getDescription());
    }

    @Test
    void testToNave() {
        NaveDTO naveDTO = new NaveDTO();
        naveDTO.setId(1L);
        naveDTO.setName("Nave 1");
        naveDTO.setSeries("Series 1");
        naveDTO.setDescription("Descripción 1");

        Nave resultNave = naveMapper.toNave(naveDTO);

        assertEquals(naveDTO.getId(), resultNave.getId());
        assertEquals(naveDTO.getName(), resultNave.getName());
        assertEquals(naveDTO.getSeries(), resultNave.getSeries());
        assertEquals(naveDTO.getDescription(), resultNave.getDescription());
    }
}
