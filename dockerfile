FROM openjdk:17-jdk-alpine
LABEL maintainer="Cristian Rogante"
EXPOSE 8080
COPY target/naves-api.jar /app/naves-api.jar
ENTRYPOINT ["java", "-jar", "/app/naves-api.jar"]
